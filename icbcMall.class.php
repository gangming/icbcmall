<?php
namespace Web\Controller\External;

use Web\Controller;
use Web\Common\Controller\MybaseController;

/**
*工行融E购票务接口

退单状态
1:待卖家同意退款
2:待卖家退款
3:待买家退还商品
4:待卖家确认收货
6:待买家修改退款信息
7:待买家确认退款
8:退款完成
9:拒绝退款
10:退款关闭
11:待运营处理争议（仅担保支付时有意义）
12:待买家补充材料（仅担保支付时有意义）
13:待卖家补充材料（仅担保支付时有意义）
14:争议驳回（仅担保支付时有意义：等同于退款拒绝，但不允许再次发起争议处理）
15:退款处理中(仅担保支付时有意义：中间状态，银行校验中，无需商户操作)
16:待卖家再次同意退款（仅担保支付时有意义：商户同意退款后银行从中间账户退款失败，需要商户再次同意）

*/
class IcbcMall extends MybaseController
{

	public $config = array('IS_API' => true);

    protected $API_NAME = 'icbcmall';

    /**
     * [$app_key 工行分配给企业对接电商平台的应用标识]
     * @var [string]
     */
    protected $app_key;

    /**
     * [$app_secret 工行分配给企业对接电商平台的应用密钥]
     * @var [string]
     */
    protected $app_secret;

    /**
     * [$auth_code 商户授权该企业应用允许访问商户数据的授权码]
     * @var [string]
     */
    protected $auth_code;

    /**
     * [$timestamp //时间戳，格式为yyyy-MM-dd HH:mm:ss.xxxxxx，API服务端允许客户端请求时间误差为前后各5分钟。时间戳最少上送到秒级。]
     * @var [string]
     */
    protected $timestamp;

    /**
     * [$req_sid 请求流水号唯一]
     * @var [string]
     */
    protected $req_sid;

    /**
     * [$url 请求url]
     * @var [string]
     */
    protected $url;

    function __construct()
    {
        list($sec, $time) = explode(' ', microtime());
        $sec = (float)$sec * 1000000;
        $this->timestamp = date('Y-m-d H:i:s.',$time).$sec;
        $this->req_sid = date('YmdHis').rand(100000,999999);
    }

    /**
     * [request 向工行电商平台发出请求 http post]
     * @date   2017-04-26
     * @param  [sting]     $method  [请求方法名称]
     * @param  [sting]     $url  [请求地址]
     * @param  [array]     $data [请求body体]
     * @return [type]           [description]
     */
    public function request($method, $data)
    {
        $commonData = [
                        'method'    => $method,
                        'req_sid'   => $this->req_sid,
                        'version'   => '',
                        'format'    => 'xml',
                        'timestamp' => $this->timestamp,
                        'app_key'   => $this->app_key,
                        'auth_code' => $this->auth_code,
                    ];
        $commonData['req_data'] = $this->buildXmlBody($data);
        $commonData['sign']     = $this->makeSign($req_data);
        $requestString          = http_build_query($commonData);
        $result = curl_post($this->url, $requestString);
        return $result;
    }

    /**
     * [makeSign SHA-256的HMAC散列算法（密钥为32位app_secret）签名后使用base64_encode]
     * @date   2017-04-26
     * @param  [xml]     $req_data []
     * @return [string]           []
     */
    private function makeSign($req_data)
    {
        $in_charset = mb_detect_encoding($req_data);
        $req_data = iconv($in_charset, 'utf-8', $req_data);
        $signStr = 'app_key='.$this->app_key.'auth_code='.$this->auth_code.'req_data='.$req_data;
        return base64_encode(hash_hmac('sha256', $signStr, $this->app_secret));
    }

    /**
     * [buildXmlBody 组装 body xml]
     * @date   2017-04-27
     * @param  [array]     $req_data [description]
     * @return [xml]
     */
    public function buildXmlBody($req_data)
    {
        $xml = <<<xml
        <?xml version="1.0" encoding="UTF-8"?><body>
xml;
        if (!is_array($req_data) || count($req_data) <= 0)
            return $xml.'</body>';
        foreach ($req_data as $k => $v) {
            if (is_numeric($k))
                continue;
            $xml.= "<{$k}>{$v}</{$k}>";
        }
        return $xml.'</body>';
    }

    /**
     *
     * [getOrderList 查询待发货订单]
     * @date   2017-04-27
     * @param  string     $order_status  01:未付款
     *                                   02:待发货
     *                                   03:待确认收货
     *                                   04:交易完成
     *                                   05:交易取消（未付款状态下，用户/商户可以取消）
     *                                   06:交易关闭（未付款超过24小时，交易自动关闭）
     * @return [type]                   [description]
     */
    public function getOrderList($order_status='02')
    {
        $req_data = [
                    'create_start_time' => '',
                    'create_end_time'   => '',
                    'modify_time_from'  => '',
                    'modify_time_to'    => '',
                    'order_status'      => $order_status,
        ];
        $response = $this->request('icbcb2c.order.list', $req_data);
    }

    public function getRefundList()
    {
        $req_data = [
                    'create_start_time' => '',
                    'create_end_time'   => '',
                    'refund_status'     => '',
                    'order_id'          => '',
                    'order_status'      => $order_status,
        ];
        $response = $this->request('', $req_data);
    }
}